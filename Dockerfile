FROM ruby:2.1.8

RUN apt-get update && apt-get install -y \
  build-essential \

  # for postgres
  libpq-dev \
  postgresql-client-9.4 \

  # for nokogiri
  libxml2-dev \
  libxslt1-dev \

  # for capybara-webkit Qt4
  libqt4-webkit \
  libqt4-dev \
  xvfb \

  # for capybara-webkit Qt5
  # qt5-default \
  # libqt5webkit5-dev \
  # gstreamer1.0-plugins-base \
  # gstreamer1.0-tools \
  # gstreamer1.0-x \
  # xvfb \

  # for a JS runtime
  nodejs \
  npm \
  imagemagick \

  # debug tools
  vim

RUN ln -s /usr/bin/nodejs /usr/bin/node
RUN npm install bower -g

ENV APP_HOME /srv/app

ENV CI_REPORTS=shippable/testresults
ENV COVERAGE_REPORTS=shippable/codecoverage

# RUN mkdir $APP_HOME

WORKDIR $APP_HOME

ENV BUNDLE_GEMFILE=$APP_HOME/Gemfile \
    BUNDLE_JOBS=8 \
    BUNDLE_PATH=/bundle_cache
