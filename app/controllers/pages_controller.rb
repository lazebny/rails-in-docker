class PagesController < ApplicationController
  def index
    @pages = Dir['app/views/pages/*']
      .map { |f| File.basename(f, '.*') }
      .reject { |page| page == 'index' }
  end

  def show
    render %(pages/#{params[:id]})
  end
end
