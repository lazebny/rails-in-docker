module Docker
  module Compose
    class Command
      def initialize(container_name: :app, args:, **options)
        @container_name = container_name
        @args = Args.new(args)
        @options = options
      end

      def bash
        wrapper(
          cmd_str: %(bash),
          env_str: args.env_str
        )
      end

      def bundle
        wrapper(
          cmd_str: %(bundle #{args.cmd_str || 'install'}),
          env_str: args.env_str
        )
      end

      def bower
        wrapper(
          cmd_str: %(bundle exec rake 'bower:#{args.cmd_str || 'install'}[--allow-root]'),
          env_str: args.env_str
        )
      end

      def rake
        wrapper(
          cmd_str: %(bundle exec rake "#{args.cmd_str}"),
          env_str: args.env_str
        )
      end

      def rspec
        rspec_cmd =
          if File.exist?('bin/rspec')
            %(bin/rspec)
          else
            %(bundle exec rspec)
          end

        rspec_path = args.cmd_str ? %('#{args.cmd_str}') : nil
        wrapper(
          cmd_str: %(#{rspec_cmd} #{rspec_path}),
          env_str: args.env_str
        )
      end

      def call
        wrapper(
          cmd_str: args.cmd_str,
          env_str: args.env_str
        )
      end

      private

      attr_reader :container_name, :args, :options

      # Options:
      #   -d                    Detached mode: Run container in the background, print
      #                         new container name.
      #   --name NAME           Assign a name to the container
      #   --entrypoint CMD      Override the entrypoint of the image.
      #   -e KEY=VAL            Set an environment variable (can be used multiple times)
      #   -u, --user=""         Run as specified username or uid
      #   --no-deps             Don't start linked services.
      #   --rm                  Remove container after run. Ignored in detached mode.
      #   -p, --publish=[]      Publish a container's port(s) to the host
      #   --service-ports       Run command with the service's ports enabled and mapped
      #                         to the host.
      #   -T                    Disable pseudo-tty allocation. By default `docker-compose run`
      #                         allocates a TTY.
      #
      def wrapper(cmd_str:, env_str:)
        args = [
          '--rm',
          env_str
        ].join(' ')
        puts cmd = %(docker-compose run #{args} #{container_name} #{cmd_str})
        cmd
      end
    end

    class Args
      def initialize(args)
        @args = args.dup
      end

      def env_str
        env.map { |arg| "-e #{arg}" }.join(' ') if env.any?
      end

      def cmd_str
        cmd.join(' ') if cmd.any?
      end

      private

      attr_reader :args

      def cmd; args - env end
      def env; args.select { |arg| arg.include? '=' } end
    end
  end
end
